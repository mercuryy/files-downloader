const { readFile, writeFile } = require("fs/promises");
const FILES_ROUTE = "https://api.mercury.pics/users/@me/files";
const DOWNLOAD_URL = "https://beta.mercury.pics/";
const fetch = require("node-fetch");

async function getFiles(session) {
  var currentPage = [ 1 ]; // kek
  let files = [];
  let paginationSkip = 0;

  while (currentPage.length !== 0) { // lazy so
    const res = await fetch(FILES_ROUTE + `?page=${paginationSkip}`, {
      headers: {
        "Authorization": session
      }
    });
    const resData = await res.json();
    currentPage = resData.files;
    paginationSkip++;

    currentPage.forEach(file => files.push(file))
  };

  return files;
};

async function downloadFiles(files) {
  for (let i in files) {
    const file = files[i];
    const res = await fetch(DOWNLOAD_URL + file.name);
    const data = await res.buffer();
    await writeFile(`./files/${file.name}`, data);
  }
}

async function main() {
  const session = await readFile("./mercury.token", { encoding: "binary" });
  const files = await getFiles(session);

  await downloadFiles(files);
}

main();